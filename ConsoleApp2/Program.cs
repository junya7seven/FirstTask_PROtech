﻿class Programm
{
    static void Main()
    {
        Console.WriteLine("Введите строку:");
        string InputString = Console.ReadLine();
        /// Проверка на четность строки
        if (InputString.Length % 2 == 0)
        {
            int split = InputString.Length / 2;
            /// разделение на 2 части
            string first = InputString.Substring(0, split);
            string second = InputString.Substring(split);
            /// переворачивание двух строк + соединение
            string first1 = new string(first.Reverse().ToArray());
            string second1 = new string(second.Reverse().ToArray());
            Console.WriteLine(first1 + second1);
        }
        /// переворачивание нечётную строку + присвоение к изначальной строки
        else
        {
            string Istring = new string(InputString.Reverse().ToArray());
            Console.WriteLine(Istring+InputString);
        }
    }
}